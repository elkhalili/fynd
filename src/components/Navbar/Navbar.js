import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
// import i18n from "../../i18n";

const NavbarComponent = () => {
  //  const { i18n } = useTranslation();
   const { t,  i18n  } = useTranslation();


  useEffect(() => {
  const lng = navigator.language;
   i18n.changeLanguage(lng);
  }, []);
  // const lng = navigator.language;
  const handleLanguageChange = (lang) => {
    console.log("event is", lang);
     i18n.changeLanguage(lang);
  };
  return (
    <div>
      {/* <div>
        <button onClick={() => handleLanguageChange('en')}>English</button>
        <button onClick={() => handleLanguageChange('fr')}>Français</button>
      </div> */}
      {/* <select
        value={localStorage.getItem("i18nextLng")}
        onChange={handleLanguageChange}
      >
        <option value="en">English</option>
        <option value="fr">Francais</option>
      </select> */}
      <Navbar expand="lg" className="bg-body-tertiary">
        <Container fluid>
          <Navbar.Brand href="#">Navbar Logo</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0 mx-auto gap-lg-5 d-flex"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Nav.Link href="/">Acceuil</Nav.Link>
              <Nav.Link href="#action2">F.Q.A</Nav.Link>
              <Nav.Link href="#action2">Apropos</Nav.Link>
              <Nav.Link href="#action2">Contact</Nav.Link>
              {/* <NavDropdown title="Link" id="navbarScrollingDropdown">
              <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Something else here
              </NavDropdown.Item>
            </NavDropdown> */}
            </Nav>
            <Form className="d-flex justify-content-between">
              <Button
                href="/auth/signup"
                className="mx-5"
                id="btn-login"
                variant="outline-success"
              >
                Inscription
              </Button>
              <Button
                href="/auth/login"
                id="btn-login"
                variant="outline-success"
              >
                Connexion
              </Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavbarComponent;
