import React from "react";
// import Button from "react-bootstrap/Button";
import Steper from "../shared/Steper";

const Home = () => {
  return (
    <div>
      <div className="container">
        <div className="text">
          <h1>
            Nous vous aidons a retrouvez ce que <br /> vous avez perdu
          </h1>
          <p>
            Avec daya3, vous pouvez partager ce que vous avez <br />
            trouvé ou chercher ce que vous avez perdu. <br />
            Rejoignez-maintenant
          </p>
          <div className="btn-choice">
            <button href="" className="mx-5 btn-trouve">
              J'ai trouvé
            </button>
            <button href="" className="btn-perte">
              J'ai perdu
            </button>
          </div>
        </div>
        <div className="photo-home"></div>
      </div>

      <div className="container-2 container">
        <div className="photo-hands"></div>
        <div className="text" style={{ padding: "4%" }}>
          <h1>
            Trop facile de retrouvez un objet
            <br />
          </h1>
          <p>
            Communiquez facilement avec ceux qui retrouvent vos objets perdus:
            cartes de crédit, <br /> téléphones, documents et bien plus encore.
          </p>
        </div>
      </div>
       <div className="text-h5" style={{textAlign:"center"}}><h5>COMMENT DAYA3 VOUS AIDE</h5></div>
       <div className="steper-home-main">
       <Steper/>
      </div>
    </div>

  );
};

export default Home;
