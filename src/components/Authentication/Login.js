import React from 'react'
import { useForm } from 'react-hook-form';
import { API } from '../../api/api';
import axios from 'axios';
 import Cookies from 'universal-cookie';

function Login() {
    const { register, handleSubmit, formState: { errors } } = useForm();
      const onSubmit =  (data, e) => {
        console.log("data0", data)
         UserSignin(data);
        e.preventDefault(); 
            
      };



       const UserSignin = (data) =>
        {
           const cookies = new Cookies()
           console.log("data1", data);
          axios
            .post(`${API}/auth/signin`, data)
            .then((response) => {
              if (response.status === 200) {
                //console.log("tokeeeen", response.data)
                
                cookies.set("token", response.data, {path: "/", maxAge: 60 * 60 * 48});
                 window.location.href = '/';
              }
              console.log("res badr", response);
            //  dispatch(userSigninSuccess(response.data.user));
            //  dispatch(addTags(response.data.allTags))
            //  dispatch(userSignRedirect());
            })
            .catch((error) => {
              console.log("erreur login", error?.response?.data?.error)
              // dispatch(userSignupFailure(error?.response?.data?.error));
            });
        };

  return (
    <div className="Auth-form-container">
    <form className="Auth-form d-flex justify-content-center" onSubmit={(data, e) => handleSubmit(onSubmit)(data, e)}>
      <div className="Auth-form-content m-5 w-25">
        <h3 className="Auth-form-title d-flex justify-content-center">Sign In</h3>
        <div className="form-group mt-3">
          <label>Email</label>
          <input
          {...register("email", { required: "Ce champ est requis." })}
            type="text"
            className={`form-control mt-1 ${errors.email ? "is-invalid" : ""}`}
            placeholder="Email"
          />
        </div>
        <div className="form-group mt-3">
          <label>Password</label>
          <input
          {...register("password")}
            type="password"
            className="form-control mt-1"
            placeholder="Password"
          />
        </div>
        <div className="d-grid gap-2 mt-3" style={{ display: "flex" }}>
          <button type="submit" className="btn btn-primary">
            Connexion
          </button>
          <a href="/auth/signup">sign up</a>
        </div>      
        <p className="text-center mt-2">
          Forgot <a href="/auth/forgot-password">Mot de Passe</a>
        </p>
      </div>
    </form>
  </div>
)
}

export default Login
