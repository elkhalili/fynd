import React from "react";
import { useForm } from "react-hook-form";
import { API } from "../../api/api";
import axios from "axios";
// import Swal from ''

function Signup() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data, e) => {
    UserSignup(data);
    e.preventDefault();
  };
  // const headers = {
  //   'Content-Type': 'application/json',
  //   'Authorization': ''
  // }
  const UserSignup = (data) => {
    axios
      .post(`${API}/auth/signup`, data)
      .then((response) => {
        const { data } = response;
        console.log("data res", data);
        window.location.href = "/";
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="Auth-form-container">
      <form
        className="Auth-form d-flex justify-content-center"
        onSubmit={(data, e) => handleSubmit(onSubmit)(data, e)}
      >
        <div className="Auth-form-content m-5 w-25">
          <h3 className="Auth-form-title d-flex justify-content-center">
            Sign up
          </h3>

          <div className="form-group mt-3">
            <label>Nom</label>
            {/* <input
              {...register("lastname")}
              type="text"
              className="form-control mt-1"
              placeholder="Nom"
              
            /> */}
            <input
              {...register("lastname", { required: "Ce champ est requis." })}
              type="text"
              className={`form-control mt-1 ${errors.lastname ? "is-invalid" : ""}`}
              placeholder="Nom"
            />
            {errors.lastname && (
              <div className="invalid-feedback">{errors.lastname.message}</div>
            )}
          </div>

          <div className="form-group mt-3">
            <label>Prénom</label>
            <input
              {...register("firstname",  { required: "Ce champ est requis." })}
              type="text"
              className={`form-control mt-1 ${errors.firstname ? "is-invalid" : ""}`}
              placeholder="Prénom"
            />
               {errors.firstname && (
              <div className="invalid-feedback">{errors.firstname.message}</div>
            )}
          </div>

          <div className="form-group mt-3">
            <label>Email</label>
            <input
              {...register("email", { required: "Ce champ est requis." })}
              type="text"
              className={`form-control mt-1 ${errors.email ? "is-invalid" : ""}`}
              placeholder="Email"
            />
                {errors.email && (
              <div className="invalid-feedback">{errors.email.message}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              {...register("password")}
              type="password"
              className={`form-control mt-1 ${errors.firstname ? "is-invalid" : ""}`}
              placeholder="password"
            />
                {errors.firstname && (
              <div className="invalid-feedback">{errors.firstname.message}</div>
            )}
          </div>
          <div className="d-grid gap-2 mt-3" style={{ display: "flex" }}>
            <button type="submit" className="btn btn-primary">
              Connexion
            </button>
            <a href="/auth/signup">sign up</a>
          </div>
          <p className="text-center mt-2">
            Forgot <a href="/auth/forgot-password">Mot de Passe</a>
          </p>
        </div>
      </form>
    </div>
  );
}

export default Signup;
