
import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Login from './Login'
import Signup from './Signup'

function Auth() {
  return (
    <Routes>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/signup" element={<Signup />}></Route>
    </Routes>
  )
}

export default Auth
