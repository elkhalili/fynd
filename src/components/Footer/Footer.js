import React from "react";
import {
  Container,
  Row,
  Col,
  InputGroup,
  FormControl,
  Button,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

function Footer() {
  return (
    <footer className="bg-neutral-100 text-center dark:bg-neutral-600 lg:text-left">
    <div className="container p-6">
      <div className="d-flex  gap-5 footer-class">
        {/* Première section de liens */}
        <div className="social-icons"> {/* Pour s'assurer que cette section occupe deux colonnes sur large écran */}
          <h5 className="mb-2.5 font-bold uppercase text-neutral-800 dark:text-neutral-200">APP LOGO</h5>
            <h6 className="rs">RESEAUX SOCIAUX</h6>
          <div className="mt-3">
               <a href="https://facebook.com">
                 <FontAwesomeIcon
                   icon={faFacebook}
                   className="mr-3 text-light bg-black p-1"
                 />
               </a>
               <a href="https://twitter.com">
                 <FontAwesomeIcon icon={faTwitter} className="mr-3 text-light mx-4 bg-black p-1" />
               </a>
             <a href="https://instagram.com">
                <FontAwesomeIcon
                   icon={faInstagram}
                  className="mr-3 text-light bg-black p-1 "
                 />
            </a>
           </div>
           <p className="text-f">© Copyright 2023. All rights reserved. Privacy policy.</p>
       
        </div>
        {/* Deuxième section de liens */}
        <div className="question-form"> {/* Pour s'assurer que cette section occupe deux colonnes sur large écran */}
          <h5 className="">vous avez des question ?</h5>
          <input
            type="text"
            id="qu-input"
            className="form-control mt-1"
            placeholder="Email Adresse"
          />
          <textarea rows={4} cols={40} id="qu-input" className="form-control mt-3 "  placeholder="Message"/>
          <button type="submit" className="btn btn-primary mt-3 bt-class">
            Contactez Nous
          </button>
          
        </div>
      </div>
    </div>
     

  </footer>
  
    // <footer className="bg-neutral-100 text-center dark:bg-neutral-600 lg:text-left">
    //   <div className="container d-flex p-6 text-neutral-800 dark:text-neutral-200">
    //     {/* <div className="grid gap-4 lg:grid-cols-2">
    //     <div className="mb-6 md:mb-0">
    //       <h5 className="mb-2 font-medium uppercase">Footer text</h5>

    //       <p className="mb-4">
    //         Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste
    //         atque ea quis molestias. Fugiat pariatur maxime quis culpa
    //         corporis vitae repudiandae aliquam voluptatem veniam, est atque
    //         cumque eum delectus sint!
    //       </p>
    //     </div>

    //     <div className="mb-6 md:mb-0">
    //       <h5 className="mb-2 font-medium uppercase">Footer text</h5>

    //       <p className="mb-4">
    //         Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste
    //         atque ea quis molestias. Fugiat pariatur maxime quis culpa
    //         corporis vitae repudiandae aliquam voluptatem veniam, est atque
    //         cumque eum delectus sint!
    //       </p>
    //     </div>
    //   </div> */}
    //     <div className="grid-container">
    //       <div>
    //         <img src="/logo.png" alt="Logo" className="img-fluid" />
    //         <div className="mt-3">
    //           <a href="https://facebook.com">
    //             <FontAwesomeIcon
    //               icon={faFacebook}
    //               className="mr-3 text-light"
    //             />
    //           </a>
    //           <a href="https://twitter.com">
    //             <FontAwesomeIcon icon={faTwitter} className="mr-3 text-light" />
    //           </a>
    //           <a href="https://instagram.com">
    //             <FontAwesomeIcon
    //               icon={faInstagram}
    //               className="mr-3 text-light"
    //             />
    //           </a>
    //         </div>
    //       </div>
    //       <div>
    //         <InputGroup className="mb-3">
    //           <FormControl placeholder="Subscribe to our newsletter" />
    //         </InputGroup>
    //         <textarea
    //           className="form-control mb-3"
    //           rows="3"
    //           placeholder="Message"
    //         ></textarea>
    //         <Button variant="primary" className="mb-3">
    //           Send
    //         </Button>
    //       </div>
    //     </div>
    //   </div>

    //   {/* <!--Copyright section--> */}
    //   <div className="bg-neutral-200 p-4 text-center text-neutral-700 dark:bg-neutral-700 dark:text-neutral-200">
    //     © 2023 Copyright:
    //     <a
    //       className="text-neutral-800 dark:text-neutral-400"
    //       href="https://tw-elements.com/"
    //     >
    //       TW Elements
    //     </a>
    //   </div>
    // </footer>
    // <div>
    //    <footer className="bg-dark text-light">
    //   <Container>
    //     <Row>
    //       <Col md={6} className="text-center text-md-left">
    //         <img src="/logo.png" alt="Logo" className="img-fluid" />
    //         <div className="mt-3">
    //           <a href="https://facebook.com"><FontAwesomeIcon icon={faFacebook} className="mr-3 text-light" /></a>
    //           <a href="https://twitter.com"><FontAwesomeIcon icon={faTwitter} className="mr-3 text-light" /></a>
    //           <a href="https://instagram.com"><FontAwesomeIcon icon={faInstagram} className="mr-3 text-light" /></a>
    //         </div>
    //       </Col>
    //       <Col md={6} className="mt-3 mt-md-0">
    //         <InputGroup className="mb-3">
    //           <FormControl placeholder="Subscribe to our newsletter" />
    //         </InputGroup>
    //         <textarea className="form-control mb-3" rows="3" placeholder="Message"></textarea>
    //         <Button variant="primary" className="mb-3">Send</Button>
    //       </Col>
    //     </Row>
    //   </Container>
    //   <div className="bg-secondary text-center py-2">
    //     <Container>
    //       <p className="m-0">© {new Date().getFullYear()} Your Company. All rights reserved.</p>
    //     </Container>
    //   </div>
    // </footer>
    // </div>
  );
}

export default Footer;
