import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./components/Home/Home";
import NavbarComponent from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Auth from "./components/Authentication/Auth";
import { Suspense } from "react";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";

function App() {
  const { t  } = useTranslation();
  return (
    <Suspense fallback={null}>
      <div>
        <div>
        {/* <h>{t("greeting")}</h> */}
        </div>
        <NavbarComponent />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/auth/*" element={<Auth />} />
          </Routes>
        </BrowserRouter>
        <Footer />
      </div>
    </Suspense>
  );
}

export default App;
