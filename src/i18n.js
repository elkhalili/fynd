import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
// import HttpBackend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  // .use(HttpBackend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    debug: true,
    // supportedLngs: ["en", "fr", "ar"], // *** added this ***
    // ns: ['translations'],
    // interpolation: {
    //   escapeValue: false, // not needed for react as it escapes by default
    // },
    // react: {
    //   useSuspense: false,
    // },
    ressources: {
           en: {
       translation: {
        greeting: "heeey"
        }
       },
    },
  //   HttpBackend: {
  //     loadPath: `http://localhost:4000/locales/{{lng}}/{{ns}}.json`,
  // },
    // backend: {
    //     loadPath: './locales/{{lng}}/translation.json', // Chemin pour charger les fichiers de traduction depuis le serveur
    //   },
  });

export default i18n;